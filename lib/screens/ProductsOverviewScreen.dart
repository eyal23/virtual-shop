import "package:flutter/material.dart";
import "package:provider/provider.dart";

import '../providers/CartProvider.dart';
import '../providers/ProductsProvider.dart';
import '../screens/CartScreen.dart';
import "../widgets/ProductsGrid.dart";
import "../widgets/badge.dart";
import '../widgets/AppDrawer.dart';

enum FilterOptions {
  Favorites,
  All,
}

class ProductsOverviewScreen extends StatefulWidget {
  ProductsOverviewScreen();

  @override
  _ProductsOverviewScreenState createState() => _ProductsOverviewScreenState();
}

class _ProductsOverviewScreenState extends State<ProductsOverviewScreen> {
  bool _showFavoritesOnly = false;
  bool _isLoading = true;

  @override
  void initState() {
    super.initState();
    Provider.of<ProductsProvider>(
      context,
      listen: false,
    )
        .fetchAndSetProducts()
        .then((_) => this.setState(() => this._isLoading = false));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("MyShop"),
        actions: [
          PopupMenuButton(
            onSelected: (FilterOptions selectedValue) {
              this.setState(() {
                if (selectedValue == FilterOptions.Favorites) {
                  this._showFavoritesOnly = true;
                } else {
                  this._showFavoritesOnly = false;
                }
              });
            },
            icon: Icon(Icons.more_vert),
            itemBuilder: (_) => [
              PopupMenuItem(
                child: Text("Only Favorites"),
                value: FilterOptions.Favorites,
              ),
              PopupMenuItem(
                child: Text("Show All"),
                value: FilterOptions.All,
              ),
            ],
          ),
          Consumer<CartProvider>(
            builder: (_, cartProvider, child) => Badge(
              child: child,
              value: cartProvider.itemCount.toString(),
            ),
            child: IconButton(
              icon: Icon(Icons.shopping_cart),
              onPressed: () {
                Navigator.pushNamed(
                  context,
                  CartScreen.routeName,
                );
              },
            ),
          ),
        ],
      ),
      body: this._isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : ProductsGrid(this._showFavoritesOnly),
      drawer: AppDrawer(),
    );
  }
}
