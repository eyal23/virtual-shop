import "package:flutter/material.dart";
import 'package:provider/provider.dart';

import "../widgets/AppDrawer.dart";
import '../widgets/UserProductItem.dart';
import '../providers/ProductsProvider.dart';
import '../screens/EditProductScreen.dart';

class UserProductsScreen extends StatelessWidget {
  static const String routeName = "/user-products";

  const UserProductsScreen();

  Future<void> _refreshProducts(BuildContext context) async {
    await Provider.of<ProductsProvider>(
      context,
      listen: false,
    ).fetchAndSetProducts(true);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Your Products"),
        actions: [
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () {
              Navigator.of(context).pushNamed(EditProductScreen.routeName);
            },
          ),
        ],
      ),
      body: FutureBuilder(
        future: this._refreshProducts(context),
        builder: (ctx, snapshot) =>
            snapshot.connectionState == ConnectionState.waiting
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : RefreshIndicator(
                    onRefresh: () => this._refreshProducts(context),
                    child: Consumer<ProductsProvider>(
                      builder: (ctx, productsProvider, _) => Padding(
                        padding: const EdgeInsets.all(8),
                        child: ListView.builder(
                          itemCount: productsProvider.items.length,
                          itemBuilder: (_, index) => Column(
                            children: [
                              UserProductItem(
                                productsProvider.items[index].id,
                                productsProvider.items[index].title,
                                productsProvider.items[index].imageUrl,
                              ),
                              Divider(),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
      ),
      drawer: AppDrawer(),
    );
  }
}
