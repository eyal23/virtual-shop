import "package:flutter/material.dart";
import "package:provider/provider.dart";

import '../providers/CartProvider.dart' show CartProvider;
import "../providers/OrdersProvider.dart";
import "../widgets/CartItem.dart";

class CartScreen extends StatelessWidget {
  static const String routeName = "/cart";

  const CartScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final cartProvider = Provider.of<CartProvider>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("Your Cart"),
      ),
      body: Column(
        children: [
          Card(
            margin: const EdgeInsets.all(15),
            child: Padding(
              padding: const EdgeInsets.all(8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Total",
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                  Spacer(),
                  Chip(
                    label: Text(
                      "\$${cartProvider.totalAmount.toStringAsFixed(2)}",
                      style: TextStyle(
                        color:
                            Theme.of(context).primaryTextTheme.headline6.color,
                      ),
                    ),
                    backgroundColor: Theme.of(context).primaryColor,
                  ),
                  OrderButton(cartProvider),
                ],
              ),
            ),
          ),
          SizedBox(height: 10),
          Expanded(
            child: ListView.builder(
              itemCount: cartProvider.itemCount,
              itemBuilder: (ctx, index) => CartItem(
                id: cartProvider.items.values.toList()[index].id,
                title: cartProvider.items.values.toList()[index].title,
                quantity: cartProvider.items.values.toList()[index].quantity,
                price: cartProvider.items.values.toList()[index].price,
                productId: cartProvider.items.keys.toList()[index],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class OrderButton extends StatefulWidget {
  const OrderButton(this.cartProvider);

  final CartProvider cartProvider;

  @override
  _OrderButtonState createState() => _OrderButtonState();
}

class _OrderButtonState extends State<OrderButton> {
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: widget.cartProvider.totalAmount <= 0 || this._isLoading
          ? null
          : () async {
              this.setState(() => this._isLoading = true);
              await Provider.of<OrdersProvider>(
                context,
                listen: false,
              ).addOrder(
                widget.cartProvider.items.values.toList(),
                widget.cartProvider.totalAmount,
              );
              this.setState(() => this._isLoading = false);
              widget.cartProvider.clear();
            },
      child: this._isLoading
          ? CircularProgressIndicator()
          : Text(
              "ORDER NOW",
              style: TextStyle(
                color: Theme.of(context).primaryColor,
              ),
            ),
    );
  }
}
