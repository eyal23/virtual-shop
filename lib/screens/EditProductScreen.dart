import "package:flutter/material.dart";
import 'package:provider/provider.dart';

import '../providers/ProductsProvider.dart';
import "../providers/ProductProvider.dart";

class EditProductScreen extends StatefulWidget {
  static const String routeName = "/edit-product";

  EditProductScreen();

  @override
  _EditProductScreenState createState() => _EditProductScreenState();
}

class _EditProductScreenState extends State<EditProductScreen> {
  final _priceFocusNode = FocusNode();
  final _descriptionFocusNode = FocusNode();
  final _imageUrlFocusNode = FocusNode();
  final _imageUrlController = TextEditingController();
  final _form = GlobalKey<FormState>();
  var _editedProduct = ProductProvider(
    id: null,
    title: "",
    description: "",
    price: 0,
    imageUrl: "",
  );
  var _initialValues = {"title": "", "description": "", "price": ""};
  bool _isInit = true;
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    this._imageUrlFocusNode.addListener(this._updateImageUrl);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (this._isInit) {
      final productId = ModalRoute.of(context).settings.arguments as String;
      if (productId != null) {
        this._editedProduct =
            Provider.of<ProductsProvider>(context).findById(productId);
        this._initialValues = {
          "title": this._editedProduct.title,
          "description": this._editedProduct.description,
          "price": this._editedProduct.price.toString(),
        };
        this._imageUrlController.text = this._editedProduct.imageUrl;
      }
    }
    this._isInit = false;
  }

  @override
  void dispose() {
    this._imageUrlFocusNode.removeListener(this._updateImageUrl);
    this._priceFocusNode.dispose();
    this._descriptionFocusNode.dispose();
    this._imageUrlFocusNode.dispose();
    this._imageUrlController.dispose();
    super.dispose();
  }

  void _updateImageUrl() {
    if (!this._imageUrlFocusNode.hasFocus) {
      if ((!this._imageUrlController.text.startsWith("http") &&
              !this._imageUrlController.text.startsWith("https")) ||
          (!this._imageUrlController.text.endsWith(".png") &&
              !this._imageUrlController.text.endsWith(".jpg") &&
              !this._imageUrlController.text.endsWith(".jpeg"))) {
        return;
      }
      setState(() {});
    }
  }

  Future<void> _saveForm() async {
    final isValid = this._form.currentState.validate();
    if (!isValid) {
      return;
    }

    this._form.currentState.save();
    this.setState(() => this._isLoading = true);
    try {
      if (this._editedProduct.id != null) {
        await Provider.of<ProductsProvider>(context, listen: false)
            .updateProduct(this._editedProduct.id, this._editedProduct);
      } else {
        await Provider.of<ProductsProvider>(context, listen: false)
            .addProduct(this._editedProduct);
      }
    } catch (error) {
      await showDialog<Null>(
        context: context,
        builder: (ctx) => AlertDialog(
          title: Text("An error occured!"),
          content: Text("Something went wrong!"),
          actions: [
            TextButton(
              onPressed: () => Navigator.of(ctx).pop(),
              child: Text("Okay"),
            ),
          ],
        ),
      );
    } finally {
      Navigator.of(context).pop();
      this.setState(() => this._isLoading = false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit Product"),
        actions: [
          IconButton(
            icon: Icon(Icons.save),
            onPressed: this._saveForm,
          )
        ],
      ),
      body: this._isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Padding(
              padding: const EdgeInsets.all(16),
              child: Form(
                key: this._form,
                child: ListView(
                  children: [
                    TextFormField(
                      initialValue: this._initialValues["title"],
                      decoration: InputDecoration(
                        labelText: "Title",
                      ),
                      textInputAction: TextInputAction.next,
                      onFieldSubmitted: (_) {
                        FocusScope.of(context)
                            .requestFocus(this._priceFocusNode);
                      },
                      onSaved: (title) => this._editedProduct = ProductProvider(
                        id: this._editedProduct.id,
                        title: title,
                        description: this._editedProduct.description,
                        price: this._editedProduct.price,
                        imageUrl: this._editedProduct.imageUrl,
                        isFavorite: this._editedProduct.isFavorite,
                      ),
                      validator: (value) =>
                          value.isEmpty ? "Please enter a title." : null,
                    ),
                    TextFormField(
                      initialValue: this._initialValues["price"],
                      decoration: InputDecoration(
                        labelText: "Price",
                      ),
                      textInputAction: TextInputAction.next,
                      keyboardType: TextInputType.number,
                      focusNode: this._priceFocusNode,
                      onFieldSubmitted: (_) {
                        FocusScope.of(context)
                            .requestFocus(this._descriptionFocusNode);
                      },
                      onSaved: (price) => this._editedProduct = ProductProvider(
                        id: this._editedProduct.id,
                        title: this._editedProduct.title,
                        description: this._editedProduct.description,
                        price: double.parse(price),
                        imageUrl: this._editedProduct.imageUrl,
                        isFavorite: this._editedProduct.isFavorite,
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return "Please enter a price.";
                        }
                        if (double.tryParse(value) == null) {
                          return "Please enter a valid price.";
                        }
                        if (double.parse(value) <= 0) {
                          return "Please enter a positive price.";
                        }

                        return null;
                      },
                    ),
                    TextFormField(
                      initialValue: this._initialValues["description"],
                      decoration: InputDecoration(
                        labelText: "Description",
                      ),
                      maxLines: 3,
                      keyboardType: TextInputType.multiline,
                      focusNode: this._descriptionFocusNode,
                      onSaved: (description) =>
                          this._editedProduct = ProductProvider(
                        id: this._editedProduct.id,
                        title: this._editedProduct.title,
                        description: description,
                        price: this._editedProduct.price,
                        imageUrl: this._editedProduct.imageUrl,
                        isFavorite: this._editedProduct.isFavorite,
                      ),
                      validator: (value) => value.isEmpty
                          ? "Please provide a description."
                          : null,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Container(
                          width: 100,
                          height: 100,
                          margin: const EdgeInsets.only(
                            top: 8,
                            right: 10,
                          ),
                          padding: const EdgeInsets.all(4),
                          decoration: BoxDecoration(
                            border: Border.all(
                              width: 1,
                              color: Colors.grey,
                            ),
                          ),
                          child: this._imageUrlController.text.isEmpty
                              ? Center(
                                  child: Text("Enter a URL"),
                                )
                              : FittedBox(
                                  child: Image.network(
                                    this._imageUrlController.text,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                        ),
                        Expanded(
                          child: TextFormField(
                            decoration: InputDecoration(
                              labelText: "Image URL",
                            ),
                            keyboardType: TextInputType.url,
                            textInputAction: TextInputAction.done,
                            controller: this._imageUrlController,
                            focusNode: this._imageUrlFocusNode,
                            onEditingComplete: () {
                              if ((!this
                                          ._imageUrlController
                                          .text
                                          .startsWith("http") &&
                                      !this
                                          ._imageUrlController
                                          .text
                                          .startsWith("https")) ||
                                  (!this
                                          ._imageUrlController
                                          .text
                                          .endsWith(".png") &&
                                      !this
                                          ._imageUrlController
                                          .text
                                          .endsWith(".jpg") &&
                                      !this
                                          ._imageUrlController
                                          .text
                                          .endsWith(".jpeg"))) {
                                return;
                              }
                              this.setState(() {});
                            },
                            onFieldSubmitted: (_) => this._saveForm(),
                            onSaved: (imageUrl) =>
                                this._editedProduct = ProductProvider(
                              id: this._editedProduct.id,
                              title: this._editedProduct.title,
                              description: this._editedProduct.description,
                              price: this._editedProduct.price,
                              imageUrl: imageUrl,
                              isFavorite: this._editedProduct.isFavorite,
                            ),
                            validator: (value) {
                              if (value.isEmpty) {
                                return "Please enter an image URL.";
                              }
                              if (!value.startsWith("http") &&
                                  !value.startsWith("https")) {
                                return "Please enter a valid URL.";
                              }
                              if (!value.endsWith(".png") &&
                                  !value.endsWith(".jpg") &&
                                  !value.endsWith(".jpeg")) {
                                return "Please enter a valid image URL.";
                              }

                              return null;
                            },
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
    );
  }
}
