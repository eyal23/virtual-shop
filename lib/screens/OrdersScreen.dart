import "package:flutter/material.dart";
import 'package:provider/provider.dart';

import "../providers/OrdersProvider.dart" show OrdersProvider;
import "../widgets/OrderItem.dart";
import '../widgets/AppDrawer.dart';

class OrdersScreen extends StatelessWidget {
  static const String routeName = "/orders";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Your Orders"),
      ),
      body: FutureBuilder(
        future: Provider.of<OrdersProvider>(
          context,
          listen: false,
        ).fetchAndSetOrders(),
        builder: (ctx, dataSnapshot) {
          if (dataSnapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (dataSnapshot.error != null) {
            return Center(
              child: Text("An error occurred!"),
            );
          }
          return Consumer<OrdersProvider>(
            builder: (ctx, ordersProvider, child) => ListView.builder(
              itemCount: ordersProvider.orders.length,
              itemBuilder: (_, index) => OrderItem(
                ordersProvider.orders[index],
              ),
            ),
          );
        },
      ),
      drawer: AppDrawer(),
    );
  }
}
