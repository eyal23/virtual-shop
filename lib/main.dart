import "package:flutter/material.dart";
import "package:provider/provider.dart";

import './screens/OrdersScreen.dart';
import "./screens/ProductsOverviewScreen.dart";
import "./screens/ProductDetailScreen.dart";
import "./screens/CartScreen.dart";
import "./screens/UserProductsScreen.dart";
import "./screens/EditProductScreen.dart";
import "./screens/AuthScreen.dart";
import 'screens/SplashScreen.dart';
import "./providers/ProductsProvider.dart";
import "./providers/CartProvider.dart";
import "./providers/OrdersProvider.dart";
import "./providers/AuthProvider.dart";

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (ctx) => AuthProvider(),
        ),
        ChangeNotifierProxyProvider<AuthProvider, ProductsProvider>(
          update: (ctx, authProvider, previousProducts) => ProductsProvider(
            authProvider.token,
            authProvider.userId,
            previousProducts.items == null ? [] : previousProducts.items,
          ),
          create: (_) => ProductsProvider(null, null, []),
        ),
        ChangeNotifierProvider(
          create: (ctx) => CartProvider(),
        ),
        ChangeNotifierProxyProvider<AuthProvider, OrdersProvider>(
          update: (ctx, authProvider, previousOrders) => OrdersProvider(
            authProvider.token,
            authProvider.userId,
            previousOrders.orders == null ? [] : previousOrders.orders,
          ),
          create: (_) => OrdersProvider(null, null, []),
        ),
      ],
      child: Consumer<AuthProvider>(
        builder: (ctx, authProvider, _) => MaterialApp(
          title: "MyShop",
          theme: ThemeData(
            primarySwatch: Colors.purple,
            accentColor: Colors.orange,
            fontFamily: "Lato",
          ),
          home: authProvider.isAuth
              ? ProductsOverviewScreen()
              : FutureBuilder(
                  future: authProvider.tryAutoLogin(),
                  builder: (ctx, snapshot) =>
                      snapshot.connectionState == ConnectionState.waiting
                          ? SplashScreen()
                          : AuthScreen(),
                ),
          routes: {
            ProductDetailScreen.routeName: (ctx) => ProductDetailScreen(),
            CartScreen.routeName: (ctx) => CartScreen(),
            OrdersScreen.routeName: (ctx) => OrdersScreen(),
            UserProductsScreen.routeName: (ctx) => UserProductsScreen(),
            EditProductScreen.routeName: (ctx) => EditProductScreen(),
          },
        ),
      ),
    );
  }
}
