import "package:flutter/material.dart";
import 'package:provider/provider.dart';

import '../providers/AuthProvider.dart';
import '../providers/ProductProvider.dart';
import '../providers/CartProvider.dart';
import "../screens/ProductDetailScreen.dart";

class ProductItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final productProvider = Provider.of<ProductProvider>(
      context,
      listen: false,
    );
    final cartProvider = Provider.of<CartProvider>(
      context,
      listen: false,
    );
    final authProvider = Provider.of<AuthProvider>(
      context,
      listen: false,
    );

    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: GridTile(
        child: GestureDetector(
          onTap: () {
            Navigator.of(context).pushNamed(
              ProductDetailScreen.routeName,
              arguments: productProvider.id,
            );
          },
          child: Hero(
            tag: productProvider.id,
            child: FadeInImage(
              placeholder: AssetImage("assets/images/productPlaceholder.png"),
              image: NetworkImage(productProvider.imageUrl),
              fit: BoxFit.cover,
            ),
          ),
        ),
        footer: GridTileBar(
          backgroundColor: Colors.black87,
          leading: Consumer<ProductProvider>(
            builder: (ctx, productProvider, _) => IconButton(
              icon: Icon(
                productProvider.isFavorite
                    ? Icons.favorite
                    : Icons.favorite_border,
              ),
              onPressed: () {
                productProvider.toggleFavoriteStatus(
                  authProvider.token,
                  authProvider.userId,
                );
              },
              color: Theme.of(context).accentColor,
            ),
          ),
          title: Text(
            productProvider.title,
            textAlign: TextAlign.center,
          ),
          trailing: IconButton(
            icon: Icon(Icons.shopping_cart),
            onPressed: () {
              cartProvider.addItem(
                productProvider.id,
                productProvider.price,
                productProvider.title,
              );
              ScaffoldMessenger.of(context).hideCurrentSnackBar();
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: Text("Added item to cart!"),
                  duration: Duration(seconds: 2),
                  action: SnackBarAction(
                    label: "UNDO",
                    onPressed: () =>
                        cartProvider.removeSingleItem(productProvider.id),
                  ),
                ),
              );
            },
            color: Theme.of(context).accentColor,
          ),
        ),
      ),
    );
  }
}
