import 'dart:math';

import "package:flutter/material.dart";
import "package:intl/intl.dart";

import "../providers/OrdersProvider.dart" as ord;

class OrderItem extends StatefulWidget {
  final ord.OrderItem order;

  const OrderItem(this.order);

  @override
  _OrderItemState createState() => _OrderItemState();
}

class _OrderItemState extends State<OrderItem> {
  bool _expanded = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Card(
        margin: const EdgeInsets.all(10),
        child: Column(
          children: [
            ListTile(
              title: Text("\$${this.widget.order.amount}"),
              subtitle: Text(
                DateFormat("dd/MM/yyyy hh:mm")
                    .format(this.widget.order.dateTime),
              ),
              trailing: IconButton(
                icon: Icon(
                  this._expanded ? Icons.expand_less : Icons.expand_more,
                ),
                onPressed: () => setState(
                  () => this._expanded = !this._expanded,
                ),
              ),
            ),
            if (this._expanded)
              AnimatedContainer(
                curve: Curves.bounceInOut,
                duration: Duration(milliseconds: 300),
                padding: const EdgeInsets.symmetric(
                  horizontal: 15,
                  vertical: 4,
                ),
                height: this._expanded
                    ? min(
                        this.widget.order.products.length * 20.0 + 10,
                        180,
                      )
                    : 0,
                child: ListView(
                  children: this
                      .widget
                      .order
                      .products
                      .map((product) => Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                product.title,
                                style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Text(
                                "${product.quantity}x \$${product.price}",
                                style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.grey,
                                ),
                              ),
                            ],
                          ))
                      .toList(),
                ),
              )
          ],
        ),
      ),
    );
  }
}
