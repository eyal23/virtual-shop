import "dart:convert";

import "package:flutter/material.dart";
import "package:http/http.dart" as http;

import './ProductProvider.dart';
import "../models/HttpException.dart";

class ProductsProvider with ChangeNotifier {
  List<ProductProvider> _items = [];
  final String authToken;
  final String userId;

  ProductsProvider(this.authToken, this.userId, this._items);

  List<ProductProvider> get items => [...this._items];

  List<ProductProvider> get favoriteItems =>
      this._items.where((product) => product.isFavorite).toList();

  ProductProvider findById(String id) =>
      this._items.firstWhere((product) => product.id == id);

  Future<void> fetchAndSetProducts([bool filterByUser = false]) async {
    final params = filterByUser
        ? {
            "auth": this.authToken,
            "orderBy": "\"creatorId\"",
            "equalTo": "\"$userId\"",
          }
        : {
            "auth": this.authToken,
          };
    final url = Uri.https(
      "shop-app-cf10e-default-rtdb.firebaseio.com",
      "/products.json",
      params,
    );
    final response = await http.get(url);
    final extractedData = json.decode(response.body) as Map<String, dynamic>;
    final List<ProductProvider> loadedProducts = [];
    if (extractedData == null) {
      return;
    }
    final favortieParams = {
      "auth": this.authToken,
    };
    final favoriteUrl = Uri.https(
      "shop-app-cf10e-default-rtdb.firebaseio.com",
      "/userFavorites/$userId.json",
      favortieParams,
    );
    final favoriteResponse = await http.get(favoriteUrl);
    final favoriteData = json.decode(favoriteResponse.body);
    extractedData.forEach(
      (prodId, prodData) => loadedProducts.add(
        ProductProvider(
          id: prodId,
          title: prodData["title"],
          description: prodData["description"],
          price: prodData["price"],
          imageUrl: prodData["imageUrl"],
          isFavorite:
              favoriteData == null ? false : favoriteData[prodId] ?? false,
        ),
      ),
    );
    this._items = loadedProducts;
    notifyListeners();
  }

  Future<void> addProduct(ProductProvider productProvider) async {
    final params = {
      "auth": this.authToken,
    };
    final url = Uri.https(
      "shop-app-cf10e-default-rtdb.firebaseio.com",
      "/products.json",
      params,
    );
    final response = await http.post(
      url,
      body: json.encode({
        "title": productProvider.title,
        "description": productProvider.description,
        "imageUrl": productProvider.imageUrl,
        "price": productProvider.price,
        "creatorId": this.userId,
      }),
    );
    final newProductProvider = ProductProvider(
      id: json.decode(response.body)["name"],
      title: productProvider.title,
      description: productProvider.description,
      price: productProvider.price,
      imageUrl: productProvider.imageUrl,
    );
    this._items.add(newProductProvider);
    this.notifyListeners();
  }

  Future<void> deleteProduct(String id) async {
    final params = {
      "auth": this.authToken,
    };
    final url = Uri.https(
      "shop-app-cf10e-default-rtdb.firebaseio.com",
      "/products/$id.json",
      params,
    );
    final existingProductIndex =
        this._items.indexWhere((prod) => prod.id == id);
    var existingProduct = this._items[existingProductIndex];
    this._items.removeWhere((product) => product.id == id);
    notifyListeners();
    final response = await http.delete(url);
    if (response.statusCode >= 400) {
      this._items.insert(existingProductIndex, existingProduct);
      notifyListeners();
      throw HttpException("Couldn't delete product.");
    }
    existingProduct = null;
  }

  Future<void> updateProduct(String id, ProductProvider productProvider) async {
    final params = {
      "auth": this.authToken,
    };
    final index = this._items.indexWhere((prod) => prod.id == id);
    if (index >= 0) {
      final url = Uri.https(
        "shop-app-cf10e-default-rtdb.firebaseio.com",
        "/products/$id.json",
        params,
      );
      await http.patch(
        url,
        body: json.encode({
          "title": productProvider.title,
          "description": productProvider.description,
          "imageUrl": productProvider.imageUrl,
          "price": productProvider.price
        }),
      );
      this._items[index] = productProvider;
      notifyListeners();
    }
  }
}
