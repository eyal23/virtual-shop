import 'dart:convert';

import 'package:flutter/material.dart';
import "package:http/http.dart" as http;

class ProductProvider with ChangeNotifier {
  final String id;
  final String title;
  final String description;
  final double price;
  final String imageUrl;
  bool isFavorite;

  ProductProvider({
    @required this.id,
    @required this.title,
    @required this.description,
    @required this.price,
    @required this.imageUrl,
    this.isFavorite = false,
  });

  void _setFavoriteValue(bool newValue) {
    this.isFavorite = newValue;
    notifyListeners();
  }

  Future<void> toggleFavoriteStatus(String token, String userId) async {
    final params = {
      "auth": token,
    };
    final oldStatus = this.isFavorite;
    this.isFavorite = !this.isFavorite;
    notifyListeners();
    final url = Uri.https(
      "shop-app-cf10e-default-rtdb.firebaseio.com",
      "/userFavorites/$userId/$id.json",
      params,
    );
    try {
      final response = await http.put(
        url,
        body: json.encode(this.isFavorite),
      );
      if (response.statusCode >= 400) {
        this._setFavoriteValue(oldStatus);
      }
    } catch (error) {
      this._setFavoriteValue(oldStatus);
    }
  }
}
