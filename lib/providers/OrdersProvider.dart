import 'dart:convert';

import "package:flutter/material.dart";
import "package:http/http.dart" as http;

import "./CartProvider.dart";

class OrderItem {
  final String id;
  final double amount;
  final List<CartItem> products;
  final DateTime dateTime;

  OrderItem({
    @required this.id,
    @required this.amount,
    @required this.products,
    @required this.dateTime,
  });
}

class OrdersProvider with ChangeNotifier {
  List<OrderItem> _orders = [];
  final String authToken;
  final String userId;

  OrdersProvider(this.authToken, this.userId, this._orders);

  List<OrderItem> get orders => [...this._orders];

  Future<void> fetchAndSetOrders() async {
    final params = {
      "auth": this.authToken,
    };
    final url = Uri.https(
      "shop-app-cf10e-default-rtdb.firebaseio.com",
      "/orders/$userId.json",
      params,
    );
    final response = await http.get(url);
    final List<OrderItem> loadedOrders = [];
    final extractedData = json.decode(response.body) as Map<String, dynamic>;
    if (extractedData == null) {
      return;
    }
    extractedData.forEach((orderId, orderData) {
      loadedOrders.add(
        OrderItem(
          id: orderId,
          amount: orderData["amount"],
          products: (orderData["products"] as List<dynamic>)
              .map((item) => CartItem(
                    id: item["id"],
                    title: item["title"],
                    quantity: item["quantity"],
                    price: item["price"],
                  ))
              .toList(),
          dateTime: DateTime.parse(orderData["dateTime"]),
        ),
      );
    });
    this._orders = loadedOrders.reversed.toList();
    notifyListeners();
  }

  Future<void> addOrder(List<CartItem> cartProducts, double total) async {
    final params = {
      "auth": this.authToken,
    };
    final url = Uri.https(
      "shop-app-cf10e-default-rtdb.firebaseio.com",
      "/orders/$userId.json",
      params,
    );
    final timestamp = DateTime.now();
    final response = await http.post(
      url,
      body: json.encode({
        "amount": total,
        "dateTime": timestamp.toIso8601String(),
        "products": cartProducts
            .map((cartProd) => {
                  "id": cartProd.id,
                  "title": cartProd.title,
                  "quantity": cartProd.quantity,
                  "price": cartProd.price,
                })
            .toList(),
      }),
    );
    this._orders.insert(
          0,
          OrderItem(
            id: json.decode(response.body)["name"],
            amount: total,
            products: cartProducts,
            dateTime: timestamp,
          ),
        );
    notifyListeners();
  }
}
