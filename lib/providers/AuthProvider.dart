import "dart:convert";
import "dart:async";

import "package:flutter/material.dart";
import "package:http/http.dart" as http;
import "package:shared_preferences/shared_preferences.dart";

import "../models/HttpException.dart";

class AuthProvider with ChangeNotifier {
  String _token;
  DateTime _expiryDate;
  String _userId;
  Timer _authTimer;

  bool get isAuth {
    return this.token != null;
  }

  String get token {
    if (this._expiryDate != null &&
        this._expiryDate.isAfter(DateTime.now()) &&
        this._token != null) {
      return this._token;
    }
    return null;
  }

  String get userId => this._userId;

  Future<void> _authenticate(
    String email,
    String password,
    String urlSegment,
  ) async {
    const params = {
      'key': 'AIzaSyBPhZTr9dZVE0HXM_LyL-QGeGKMQX-YG3Y',
    };
    final url = Uri.https(
      'identitytoolkit.googleapis.com',
      '/v1/accounts:$urlSegment',
      params,
    );
    final response = await http.post(
      url,
      body: json.encode({
        "email": email,
        "password": password,
        "returnSecureToken": true,
      }),
    );
    final responseData = json.decode(response.body);
    if (responseData["error"] != null) {
      throw HttpException(responseData["error"]["message"]);
    }
    this._token = responseData["idToken"];
    this._userId = responseData["localId"];
    this._expiryDate = DateTime.now().add(
      Duration(
        seconds: int.parse(
          responseData["expiresIn"],
        ),
      ),
    );
    this.autoLogout();
    notifyListeners();
    final prefs = await SharedPreferences.getInstance();
    final userData = json.encode({
      "token": this._token,
      "userId": this._userId,
      "expiryDate": this._expiryDate.toIso8601String(),
    });
    prefs.setString("userData", userData);
  }

  Future<void> singup(String email, String password) async {
    await this._authenticate(
      email,
      password,
      'signUp',
    );
  }

  Future<void> login(String email, String password) async {
    await this._authenticate(
      email,
      password,
      'signInWithPassword',
    );
  }

  Future<bool> tryAutoLogin() async {
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey("userData")) {
      return false;
    }
    final extractedUserData =
        json.decode(prefs.getString("userData")) as Map<String, Object>;
    final expiryDate = DateTime.parse(extractedUserData["expiryDate"]);
    if (expiryDate.isBefore(DateTime.now())) {
      return false;
    }
    this._token = extractedUserData["token"];
    this._userId = extractedUserData["userId"];
    this._expiryDate = expiryDate;
    autoLogout();
    notifyListeners();
    return true;
  }

  void logout() async {
    this._token = null;
    this._userId = null;
    this._expiryDate = null;
    if (this._authTimer != null) {
      this._authTimer.cancel();
    }
    notifyListeners();
    final prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }

  void autoLogout() {
    if (this._authTimer != null) {
      this._authTimer.cancel();
    }
    final timeToExpiry = this._expiryDate.difference(DateTime.now()).inSeconds;
    this._authTimer = Timer(
      Duration(seconds: timeToExpiry),
      this.logout,
    );
  }
}
